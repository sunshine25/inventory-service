package com.programming.inventoryservice.model;
import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
@Data
@Entity
@Table(name = "inventory")
@AllArgsConstructor
@NoArgsConstructor
public class Inventory {
    @Id
    @GeneratedValue(generator="system-uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "id", columnDefinition = "VARCHAR(50)",
            updatable = false,
            nullable = false)
    private String id;
    private String skuCode;
    private Integer quantity;
    public void init() {
        this.id = NanoIdUtils.randomNanoId();
    }
}
